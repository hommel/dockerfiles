# Makefile to build docker images

REGISTRY := hommel
NONROOTUSER := user

.PHONY: help image nonroot

help : ## Show this help message.
	@echo 'usage: make [target] ...'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'


# Note the need for double $ to escape from Make and allow shell to run
nonroot : ## Build the base image that creates a non-root user.
	docker build --rm --force-rm \
		-t $(REGISTRY)/nonroot \
		--build-arg uid=$$(id -u) \
		--build-arg gid=$$(id -g) \
		--build-arg user=$(NONROOTUSER) \
		./nonroot


# Exit with error if the directory name isn't provided as an argument
# when invoking this target. See: https://stackoverflow.com/a/10858332
#
# NOTE HOWEVER THAT THIS DOESN'T APPEAR TO THROW AN ERROR AS INTENDED IF
# THE VARIABLE IS NOT SET...
image : ## Build a Dockerfile in directory DIR (e.g. DIR=telnet).
	@:$(call check_defined, DIR, directory of the Dockefile)
	docker build --rm --force-rm \
		-t $(REGISTRY)/$(DIR) \
		--build-arg user=$(NONROOTUSER) \
		./$(DIR)


# TODO: generate alias file from all alias files in subdirectories
